package nix.framework.spec

import geb.spock.GebReportingSpec
import nix.framework.page.BlogPage
import nix.framework.page.StartPage

class NixNavigationSpec extends GebReportingSpec {
    def "Navigate to Blog page"() {
        when:
            to StartPage
        and:
            "I navigate to Blog"()
        then:
            at BlogPage
    }
}
