package nix.framework.page

import geb.Page

class StartPage extends Page {
    static content = {
        blogLink(wait: true) { $("a", text: "Blog") }
    }
    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def "I navigate to Blog"() {
        blogLink.click()
    }
}
